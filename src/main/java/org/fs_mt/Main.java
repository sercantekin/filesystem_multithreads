package org.fs_mt;

import FileSystem.*;
import MultiThread.RunnableThreads;
/*
 *  Directory creation
 *      1. root one will be created by class constructor. no parent for them
 *      2. nested ones will be created by using create function of parent class
 *
 *  Any type of file creation
 *      1. can be created by constructor
 * */

public class Main {
    public static void main( String[] args ) throws InterruptedException {
        // directories
        Directory root = new Directory("");

        Directory home = root.create(new Directory("home"));
        Directory logs = home.create(new Directory("logs"));
        Directory downloads = home.create(new Directory("downloads"));

        Directory documents = root.create(new Directory("documents"));
        Directory user = documents.create(new Directory("user"));
        Directory streams = documents.create(new Directory("streams"));

        // log files
        Log log1 = new Log("log1", "Initial State", logs);
        Log log2 = new Log("log2", "Initial State", logs);

        // user private encrypted files
        EncText enc1 = new EncText("enc1", "Initial State", user, "leo");
        EncText enc2 = new EncText("enc2", "Initial State", user, "alan");

        // binary files
        String b1 = "01001000 01100101 01101100 01101100 01101111 \n00100000 01010111 01101111 01110010 01101100";
        String b2 = "00100000 01010111 01101111 01110010 01101100 \n01001000 01100101 01101100 01101100 01101111";

        Binary bin1 = new Binary("bin1", b1, downloads);
        Binary bin2 = new Binary("bin2", b2, downloads);

        // buffer files
        Buffer buf1 = new Buffer("buf1", streams);
        Buffer buf2 = new Buffer("buf2", streams);

        root.list();

        // threads
        RunnableThreads rt = new RunnableThreads(log1);
        Thread t1 = new Thread(rt);
        t1.setName("Thread-1");
        t1.start();

        Thread t2 = new Thread(rt);
        t2.setName("Thread-2");
        t2.start();

        Thread t3 = new Thread(rt);
        t3.setName("Thread-3");
        t3.start();

        Thread t4 = new Thread(rt);
        t4.setName("Thread-4");
        t4.start();

        Thread t5 = new Thread(rt);
        t5.setName("Thread-5");
        t5.start();
        t5.join();

        System.out.println("\n" + log1.read());
    }
}
