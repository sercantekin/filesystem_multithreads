package MultiThread;

import FileSystem.*;

import java.time.LocalDateTime;

public class RunnableThreads implements Runnable{
    private final Log node;
    private int num = 0;

    public RunnableThreads(Log node){ this.node = node; }

    /////////////////////// public methods ///////////////////////
    @Override
    public void run() {
        task_init();
        task();
        task_finalize();
    }

    /////////////////////// private methods ///////////////////////
    private synchronized void task_init(){
        // initial operation
        System.out.println(Thread.currentThread().getName() + " initializing is started");
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        node.append(Thread.currentThread().getName() + " initialized. Time: " + LocalDateTime.now());
        System.out.println(Thread.currentThread().getName() + " initializing is finished");

        // waiting all threads to finish initializing
        num++;
        while (num < 5){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private synchronized void task(){
        // performing task
        System.out.println(Thread.currentThread().getName() + " task is started");
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        node.append(Thread.currentThread().getName() + " task is completed. Time: " + LocalDateTime.now());
        System.out.println(Thread.currentThread().getName() + " task is finished");

        notify();
        // thread-3 and 4 pass first
        if (!Thread.currentThread().getName().equals("Thread-3") &&
                !Thread.currentThread().getName().equals("Thread-4")){
            try {
                Thread.sleep(750);
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private synchronized void task_finalize(){
        // thread-5 is the last one which finishes
        if (Thread.currentThread().getName().equals("Thread-5")){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // finalize the process
        System.out.println(Thread.currentThread().getName() + " finalization is started");
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        node.append(Thread.currentThread().getName() + " finalization is completed. Time: " + LocalDateTime.now());
        System.out.println(Thread.currentThread().getName() + " finalized");

        notifyAll();
    }
}
