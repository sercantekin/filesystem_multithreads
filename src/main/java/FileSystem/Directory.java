package FileSystem;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.TreeSet;

public class Directory extends Node {
    // nodes will keep elements in directory.
    // dateUpdated will be changed after each operation.
    // max is limit for keeping elements in dir
    private Set<Node> nodes;
    private int maxDirElem;
    private LocalDateTime dateUpdated;

    public Directory(String name){
        super(name);
        this.maxDirElem = 20;
        this.nodes = new TreeSet<>();
        this.dateUpdated = LocalDateTime.now();
    }

    /////////////////////// public methods ///////////////////////
    // after checking capacity and duplication, create new node in directory object
    public Directory create(Directory node){
        if (isDuplicated(node.getName())){
            System.out.println("Addition for '" + node.getName() + "' is failed: Given file name is duplicated in the directory.");
            return null;
        }else if (this.getNodes().size() < this.maxDirElem){
            this.getNodes().add(node);
            node.setParent(this);
            setDateUpdated(LocalDateTime.now());
            return node;
        }else{
            System.out.println("Directory capacity (" + this.maxDirElem + ") is full." +
                    "\nAddition for '" + node.getName() + "' is failed." +
                    "\nEither change capacity (Not recommended!) or add the file to another dir.");
            return null;
        }
    }

    // listing all elements of directory
    public void list(){
        int indent = 0;
        StringBuilder sb = new StringBuilder();
        printDirectoryTree(this, indent, sb);
        System.out.println(sb.toString());
        System.out.println("----------------------------------");
    }

    // getting details of directory
    @Override
    public String getInfo() {
        String root;
        if (this.getParent() == null){
            root = "root directory";
        }else{
            root = this.getParent().getName();
        }
        return "parent = " + root +
                ", \nname = " + this.getName() +
                ", \ncreated date = " + this.getDateCreated() +
                ", \nlast updated date = " + this.dateUpdated +
                ", \nelement consisted = " + getNodes().size() +
                ", \npath = " + this.getPath() +
                ", \nmax number of element = " + this.maxDirElem +
                "\n----------------------------------";
    }

    // get set attributes for max capacity of directory objects
    public void setMaxDirElem(int maxDirElem){ this.maxDirElem = maxDirElem; }
    int getMaxDirElem(){ return maxDirElem; }

    /////////////////////// package private methods ///////////////////////
    // get set attributes for treeSet
    void setNodes(Set<Node> nodes) { this.nodes = nodes; }
    Set<Node> getNodes() { return nodes; }

    // get set attributes for updated date
    LocalDateTime getDateUpdated() { return dateUpdated; }
    void setDateUpdated(LocalDateTime dateUpdated) { this.dateUpdated = dateUpdated; }

    // adding nodes to parent for files
    void addNode(Node node){
        this.getNodes().add(node);
    }

    /////////////////////// private methods ///////////////////////
    private void printDirectoryTree(Node node, int indent, StringBuilder sb) {
        sb.append(getIndentString(indent));
        sb.append("+--");
        sb.append(node.getName());
        if (node instanceof Directory) sb.append("/");
        sb.append("\n");

        if (node instanceof Directory) {
            Directory directory = (Directory) node;
            for (Node file : directory.getNodes()) {
                printDirectoryTree(file, indent + 1, sb);
            }
        }
    }

    private static String getIndentString(int indent) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < indent; i++) {
            sb.append("|  ");
        }
        return sb.toString();
    }

    private boolean isDuplicated(String name){
        for (Node node : this.getNodes()) {
            if (node.getName().equals(name)) return true;
        }
        return false;
    }
}
