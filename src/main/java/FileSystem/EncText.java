package FileSystem;

import Utils.EncDec;
import java.time.LocalDateTime;

public class EncText extends Log {
    private LocalDateTime dateUpdated;
    private String keyWord;

    public EncText(String name, String content, Directory parent, String keyword) {
        super(name, content, parent);
        this.setContent(EncDec.encrypt(content, keyword));
        this.setKeyWord(keyword);
        this.setName(name + ".etxt");
        this.setParent(parent);
        this.setAccess(3); // read and owner-private-write
        parent.addNode(this);
        this.dateUpdated = LocalDateTime.now();
    }

    /////////////////////// public methods ///////////////////////
    @Override
    public String getInfo() {
        String root;
        if (this.getParent() == null){
            root = "no directory";
        }else{
            root = this.getParent().getName();
        }
        return "parent = " + root +
                ", \nname = " + this.getName() +
                ", \ncreated date = " + this.getDateCreated() +
                ", \nlast updated date = " + this.dateUpdated +
                ", \npath = " + this.getPath() +
                ", \naccess = " + this.getAccess() + ": owner private"  +
                "\n----------------------------------";
    }

    // this is overwrite function of the one in Binary class
    public String read(String keyWord){
        if (keyWord.equals(this.getKeyWord())){
            return EncDec.decrypt(this.getContent(), keyWord) + "\n----------------------------------";
        }else{
            return this.getContent();
        }
    }

    // this is overwrite function of the one in Log class
    public void append(String entry, String keyWord){
        if (keyWord.equals(this.getKeyWord())){
            String nc = EncDec.decrypt(this.getContent(), keyWord) +
                    "\n" + entry;
            this.setContent(EncDec.encrypt(nc, keyWord));
            this.setDateUpdated(LocalDateTime.now());
        }else{
            System.out.println("Addition for '" + entry + "' is failed: Permission denied.");
        }
    }

    // this is overwrite function of the one in Node class
    public static boolean delete(EncText node, String keyWord){
        if (keyWord.equals(node.getKeyWord())){
            if (node.getParent() == null){
                node = null;
                System.gc();
                return true;
            }else if (node.getParent().getNodes().remove(node)){
                node.getParent().setDateUpdated(LocalDateTime.now());
                return true;
            }else{
                System.out.println("Deletion for '" + node.getName() + "' is failed: Object could not be found.");
                return false;
            }
        }else{
            System.out.println("Deletion for '" + node.getName() + "' is failed: Permission denied.");
            return false;
        }
    }

    /////////////////////// package private methods ///////////////////////
    LocalDateTime getDateUpdated() { return dateUpdated; }
    void setDateUpdated(LocalDateTime dateUpdated) { this.dateUpdated = dateUpdated; }

    /////////////////////// private methods ///////////////////////
    private String getKeyWord() { return keyWord; }
    private void setKeyWord(String keyWord) { this.keyWord = keyWord; }
}
