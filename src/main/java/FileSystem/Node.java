package FileSystem;

import java.time.LocalDateTime;

public abstract class Node implements Comparable<Node>{
    // apart from nodes in root, each node has parent
    // name and dateCreated are common for each node
    private Directory parent;
    private String name;
    private LocalDateTime dateCreated;
    private int access;

    public Node(String name){
        this.name = name;
        this.dateCreated = LocalDateTime.now();
    }

    /////////////////////// public methods ///////////////////////
    // specific for tree set. recursive function which avoids any duplication in treeSet
    @Override
    public int compareTo(Node node) { return this.getName().compareTo(node.getName()); }

    // common get info method with different functionality
    public abstract String getInfo();

    // if object is not exist, return false
    public static boolean delete(Node node){
        if (node instanceof EncText){
            System.out.println("Deletion for '" + node.getName() + "' is failed: Permission denied.");
            return false;
        }

        if (node.getParent() == null){
            node = null;
            System.gc();
            return true;
        }else if (node.getParent().getNodes().remove(node)){
            node.getParent().setDateUpdated(LocalDateTime.now());
            return true;
        }else{
            System.out.println("Deletion for '" + node.getName() + "' is failed: Object could not be found.");
            return false;
        }
    }

    // if object is not exist, moving is failed
    public void move(Directory node){
        if (delete(this)){
            this.setParent(node);
            node.getNodes().add(this);
        }else{
            System.out.println("Moving for '" + node.getName() + "' is failed. Object could not be found.");
        }
    }

    /////////////////////// package private methods ///////////////////////
    // parent directory of node
    Directory getParent() { return parent; }
    void setParent(Directory parent) { this.parent = parent; }

    // path of node
    String getPath() { return parent != null ? parent.getPath() + "/" + name : name; }

    // date it is created
    LocalDateTime getDateCreated() { return dateCreated; }

    // name of node
    public String getName() { return name; }
    void setName(String name) { this.name = name; }

    // access status
    public int getAccess() { return access; }
    public void setAccess(int access) { this.access = access; }
}
