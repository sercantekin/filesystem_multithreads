package FileSystem;

public class Binary extends Node {
    private String content;

    public Binary(String name, String content, Directory parent) {
        super(name);
        this.setName(name + ".bin");
        this.content = content;
        this.setParent(parent);
        this.setAccess(1); // read only
        parent.addNode(this);
    }

    /////////////////////// public methods ///////////////////////
    @Override
    public String getInfo() {
        String root;
        if (this.getParent() == null){
            root = "no directory";
        }else{
            root = this.getParent().getName();
        }
        return "parent = " + root +
                ", \nname = " + this.getName() +
                ", \ncreated date = " + this.getDateCreated() +
                ", \npath = " + this.getPath() +
                ", \nlength of content = " + this.getLength() +
                ", \naccess = " + this.getAccess() + ": read only"  +
                "\n----------------------------------";
    }

    public String read(){
        return this.getContent() + "\n----------------------------------";
    }

    /////////////////////// package private methods ///////////////////////
    String getContent(){ return content; }
    void setContent(String content){ this.content = content; }

    long getLength(){
        if (this.getContent() != null) return this.getContent().getBytes().length;
        return 0;
    }
}
