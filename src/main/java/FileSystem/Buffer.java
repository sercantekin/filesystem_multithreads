package FileSystem;

import java.time.LocalDateTime;

public class Buffer extends Node {
    private String content;
    private LocalDateTime dateUpdated;
    private int maxBufSize;

    public Buffer(String name, Directory parent) {
        super(name);
        this.setName(name + ".buf");
        this.setParent(parent);
        this.setContent("");
        this.dateUpdated = LocalDateTime.now();
        this.maxBufSize = 20;
        this.setAccess(2); // read and write
        parent.addNode(this);
    }

    /////////////////////// public methods ///////////////////////
    @Override
    public String getInfo() {
        String root;
        if (this.getParent() == null){
            root = "no directory";
        }else{
            root = this.getParent().getName();
        }
        return "parent = " + root +
                ", \nname = " + this.getName() +
                ", \ncreated date = " + this.getDateCreated() +
                ", \nlast updated date = " + this.dateUpdated +
                ", \ncurrent buffer size = " + this.getLength() +
                ", \npath = " + this.getPath() +
                ", \nmax capacity = " + this.maxBufSize +
                ", \naccess = " + this.getAccess() + ": read and write"  +
                "\n----------------------------------";
    }

    public void push(Character c){
        if (this.getLength() < maxBufSize){
            this.setContent(this.getContent() + c);
            setDateUpdated(LocalDateTime.now());
        }else{
            System.out.println("Buffer capacity (" + this.maxBufSize + ") is full." +
                    "\nPush operation for '" + c + "' is failed." +
                    "\nEither change capacity (Not recommended!) or consume from the buffer to push again.");
        }
    }

    public Character consume(){
        if (this.getLength() > 0){
            char c = this.getContent().charAt(0);
            StringBuilder sb = new StringBuilder(this.getContent());
            sb.deleteCharAt(0);
            this.setContent(sb.toString());
            setDateUpdated(LocalDateTime.now());
            return c;
        }else{
            System.out.println("Buffer is empty. Push first to consume from buffer.");
            return null;
        }
    }

    public void setMaxBufSize(int maxBufSize) { this.maxBufSize = maxBufSize; }

    /////////////////////// package private methods ///////////////////////
    String getContent(){ return content; }
    void setContent(String content){ this.content = content; }

    int getMaxBufSize() { return maxBufSize; }

    LocalDateTime getDateUpdated() { return dateUpdated; }
    void setDateUpdated(LocalDateTime dateUpdated) { this.dateUpdated = dateUpdated; }

    long getLength(){
        if (this.getContent() != null) return this.getContent().getBytes().length;
        return 0;
    }
}
