package FileSystem;

import java.time.LocalDateTime;

public class Log extends Binary {
    private LocalDateTime dateUpdated;
    private int numberOfEntry;

    public Log(String name, String content, Directory parent) {
        super(name, content, parent);
        this.setName(name + ".txt");
        this.setParent(parent);
        this.numberOfEntry = 1;
        this.setAccess(2); // read and write
        parent.addNode(this);
        this.dateUpdated = LocalDateTime.now();
    }

    /////////////////////// public methods ///////////////////////
    @Override
    public String getInfo() {
        String root;
        if (this.getParent() == null){
            root = "no directory";
        }else{
            root = this.getParent().getName();
        }
        return "parent = " + root +
                ", \nname = " + this.getName() +
                ", \ncreated date = " + this.getDateCreated() +
                ", \nlast updated date = " + this.dateUpdated +
                ", \nnumber of log entry = " + this.numberOfEntry +
                ", \npath = " + this.getPath() +
                ", \nlength of content = " + this.getLength() +
                ", \naccess = " + this.getAccess() + ": read and write"  +
                "\n----------------------------------";
    }

    public void append(String log){
        if (this.getAccess() == 3){
            System.out.println("Addition for '" + log + "' is failed: Permission denied.");
        }
        String nc = this.getContent() +
                "\n" + log;
        this.setContent(nc);
        this.setDateUpdated(LocalDateTime.now());
        this.numberOfEntry++;
    }

    /////////////////////// package private methods ///////////////////////
    LocalDateTime getDateUpdated() { return dateUpdated; }
    void setDateUpdated(LocalDateTime dateUpdated) { this.dateUpdated = dateUpdated; }
}
